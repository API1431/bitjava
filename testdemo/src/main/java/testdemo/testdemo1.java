
package testdemo;
import static org.junit.Assert.assertEquals;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
public class testdemo1 {
	@Before
	public void abc2() {
		System.out.println("I am BeforeEach in Junit Jupiter");
	}
	
	static OddEven obj;
	@BeforeClass
	public static void abc1() {
		obj = new OddEven();
		System.out.println("I am BeforeAll in Junit Jupiter");
	}
	
	@Test
	public void abc3() {
		assertEquals(obj.evenOdd(100), true);
		
		//assertEquals("hello", new String[]{"sfs"});
		System.out.println("I am TestCase in Junit Jupiter");
	}
	
	@Test
	public void abc8() {
		assertEquals(obj.evenOdd(101), false);
		System.out.println("I am TestCase in Junit Jupiter");
	}
	
	@After	
	public void abc4() {
		System.out.println("I am AfterEach in Junit Jupiter");
	}
	
	@AfterClass
	public static void abc5() {
		System.out.println("I am AfterAll in Junit Jupiter");
	}
	
}