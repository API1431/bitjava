package testdemo;

 

import org.junit.*;

public class Example {
	
	@BeforeClass
	public static  void abc1()
	{
		System.out.println("I am BeforeAll");
	}
	
	@Before
	public  void abc2()
	{
		System.out.println("I am BeforeEach");
	}
	
	
	@Test
	public void abc3()
	{
		System.out.println("I am TestCase");
	}

	@Test
	public void abc8()
	{
		System.out.println("I am testCase");
	}
	
	@After
	public  void abc4()
	{
		System.out.println("I am AfterEach");
	}
	
	@AfterClass
	public static void abc5()
	{
		System.out.println("I am AfterAll");
	}
}
